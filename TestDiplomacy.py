from unittest import TestCase, main
from io import StringIO

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print


class TestDiplomacy(TestCase):
    def test_diplomacy_read1(self):
        self.assertEqual(diplomacy_read("A Madrid Hold"), ["A", "Madrid", "Hold"])

    def test_diplomacy_read2(self):
        self.assertEqual(
            diplomacy_read("B Paris Support A"), ["B", "Paris", "Support", "A"]
        )

    def test_diplomacy_read3(self):
        self.assertEqual(
            diplomacy_read("C Austin Move Paris"), ["C", "Austin", "Move", "Paris"]
        )

    def test_diplomacy_solve1(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(input, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_diplomacy_solve2(self):
        input = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(input, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_diplomacy_solve3(self):
        input = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B"
        )
        w = StringIO()
        diplomacy_solve(input, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_diplomacy_solve4(self):
        input = StringIO(
            "A Madrid Move London\nB SanAntonio Hold\nC London Support B\nD Paris Move SanAntonio"
        )
        w = StringIO()
        diplomacy_solve(input, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_print1(self):
        w = StringIO()
        diplomacy_print(w, ["A", "[dead]"], ["B", "Austin"], ["C", "[dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Austin\nC [dead]\n")

    def test_diplomacy_print2(self):
        w = StringIO()
        diplomacy_print(w, ["A", "Madrid"], ["B", "Paris"], ["C", "London"])
        self.assertEqual(w.getvalue(), "A Madrid\nB Paris\nC London\n")

    def test_diplomacy_print3(self):
        w = StringIO()
        diplomacy_print(
            w, ["A", "[dead]"], ["B", "Paris"], ["C", "London"], ["D", "Madrid"]
        )
        self.assertEqual(w.getvalue(), "A [dead]\nB Paris\nC London\nD Madrid\n")


if __name__ == "__main__":  # pragma: no cover
    main()
