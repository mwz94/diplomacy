from collections import defaultdict


def check_army_name(army_name):
    assert (
        len(army_name) == 1 and army_name.isalpha() and army_name.isupper()
    ), "Invalid army name"


def check_army_location(army_location):
    assert (
        army_location.isalpha() and army_location[0].isupper()
    ), "Invalid army location"


def check_army_action(army_action):
    action = army_action[0]
    assert action in ["Hold", "Move", "Support"], "Invalid army action"
    if action == "Move":
        assert len(army_action) == 2, "Invalid army target"
        check_army_location(army_action[1])
    if action == "Support":
        assert len(army_action) == 2, "Invalid army target"
        check_army_name(army_action[1])


def diplomacy_solve(r, w):
    # FIXME: Function logic broken

    # Read army info
    army_status = {}
    for line in r:
        army_info = diplomacy_read(line)

        check_army_name(army_info[0])
        check_army_location(army_info[1])
        check_army_action(army_info[2:])

        army_status[army_info[0]] = army_info[1:] + [0]

    # Calculate support levels
    support_map = {}  # map of city: support army currently in city
    for army in army_status:
        status = army_status[army]
        if status[1] == "Support":
            target = status[2]
            army_status[target][-1] += 1
            support_map[army] = target

    # Determine army destinations
    cities = defaultdict(dict)  # map of city: {army: support level}
    for army in army_status:
        status = army_status[army]
        start = status[0]
        support_level = status[-1]
        if status[1] == "Hold" or status[1] == "Support":
            cities[start][army] = support_level
        else:
            end = status[2]
            cities[end][army] = support_level

    # Check if any supporting armies will die, and remove support accordingly
    for city in cities:
        sorted_armies = sorted(cities[city].items(), key=lambda x: x[1], reverse=True)
        max_support = sorted_armies[0][1]
        max_army = (
            ""
            if len(sorted_armies) > 1 and sorted_armies[1][1] == max_support
            else sorted_armies[0][0]
        )
        for army in cities[city]:
            if (
                cities[city][army] <= max_support
                and army != max_army
                and army in support_map
            ):
                target = support_map[army]
                target_city = (
                    army_status[target][0]
                    if army_status[target][1] == "Hold"
                    else army_status[target][2]
                )
                cities[target_city][target] -= 1

    # Determine final results
    final_status = {}
    for city in cities:
        sorted_armies = sorted(cities[city].items(), key=lambda x: x[1], reverse=True)
        all_dead = (
            True
            if len(sorted_armies) > 1 and sorted_armies[0][1] == sorted_armies[1][1]
            else False
        )

        for army, _ in sorted_armies:
            final_status[army] = "[dead]"
        if not all_dead:
            final_status[sorted_armies[0][0]] = city

    final = sorted(list(final_status.items()), key=lambda x: x[0])
    diplomacy_print(w, *final)


def diplomacy_read(line):
    return line.strip().split()


def diplomacy_print(w, *status_list):
    for status in status_list:
        w.write(status[0] + " " + status[1] + "\n")
